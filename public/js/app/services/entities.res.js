angular.module('App').factory('InvoicesRes', ['$resource', function($resource) {
  return $resource('/api/invoices/:id', null,
    {
      'get': { method:'GET', isArray: true },
      'post': { method: 'POST' },
      'details': { method: 'GET' }
    });
}])
.factory('InvoiceItemsRes', ['$resource', function($resource) {
  return $resource('/api/invoices/:invoice_id/items', null,
    {
      'get': { method:'GET', isArray: true},
      'post': { method: 'POST'}
    });
}])
.factory('CustomersRes', ['$resource', function($resource) {
  return $resource('/api/customers', null,
    {
      'get': { method:'GET', isArray: true }
    });
}])
.factory('ProductsRes', ['$resource', function($resource) {
  return $resource('/api/products', null,
    {
      'get': { method:'GET', isArray: true }
    });
}]);
