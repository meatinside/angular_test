var app = angular.module('App', [
  'ngRoute',
  'ngResource'
]).config(['$routeProvider', '$locationProvider',
  function($routeProvider, $locationProvider) {
    console.log("config");
    $routeProvider
      .when('/', {
        redirectTo: '/invoices'
      })
      .when('/invoices', {
        templateUrl: '/templates/invoices.html',
        controller: 'InvoicesCtrl',
        controllerAs: 'ctrl'
      })
      .when('/invoice/:id', {
        templateUrl: '/templates/invoice.html',
        controller: 'InvoiceCtrl',
        controllerAs: 'ctrl'
      })
      .when('/customers', {
        templateUrl: '/templates/customers.html',
        controller: 'CustomersCtrl',
        controllerAs: 'ctrl'
      })
      .when('/products', {
        templateUrl: '/templates/products.html',
        controller: 'ProductsCtrl',
        controllerAs: 'ctrl'
      });

    $locationProvider.html5Mode(true);
}]);
