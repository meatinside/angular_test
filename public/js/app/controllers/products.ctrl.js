angular.module('App').controller('ProductsCtrl', function($rootScope, $scope, ProductsRes) {

  $scope.products = [];
  $scope.loading = true;
  $rootScope.page = 'products';
  ProductsRes.get().$promise.then(function(result) {
    $scope.products = result;
    $scope.loading = false;
    console.dir(result);
  });

});
