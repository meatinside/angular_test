angular.module('App').controller('CustomersCtrl', function($rootScope, $scope, CustomersRes) {

  $scope.customers = [];
  $scope.loading = true;
  $rootScope.page = 'customers';
  CustomersRes.get().$promise.then(function(result) {
    $scope.customers = result;
    $scope.loading = false;
    console.dir(result);
  });

});
