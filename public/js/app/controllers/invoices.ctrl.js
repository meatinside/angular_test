angular.module('App').controller('InvoicesCtrl', function($rootScope, $location, $q, $scope,
  InvoicesRes, InvoiceItemsRes, ProductsRes, CustomersRes) {
  $scope.invoices = [];
  $scope.loading = true;
  $scope.showModal = false;
  $rootScope.page = 'invoices';

  // Validate modal
  $scope.$watch('modal', function(newVal) {
    if($scope.createForm) {
      $scope.createForm.$invalid = !$scope.modal.products.length;
    }
  }, true);

  // Empty form
  function emptyForm() {
    $scope.modal = {
      customer: null,
      product: null,
      products: [],
      quantity: 1,
      total: 0
    };
  }

  emptyForm();

  // Go To Invoice
  $scope.goToInvoice = function(id) {
    $location.path('/invoice/'+id);
  };

  // Open modal
  $scope.openModal = function() {
    emptyForm();
    $scope.showModal = true;
  };

  // (Modal) Close modal
  $scope.closeModal = function() {
    $scope.showModal = false;
  };

  // (Modal) Add product to list
  $scope.addProduct = function() {
    $scope.modal.products.push({
      id: $scope.modal.product,
      quantity: $scope.modal.quantity
    });
    $scope.modal.total += $scope.productsMap[$scope.modal.product].price * $scope.modal.quantity;
    $scope.modal.product = null;
    $scope.modal.quantity = 1;
  };

  // (Modal) Remove product from list
  $scope.removeProduct = function(idx) {
    var p = $scope.modal.products[idx];
    $scope.modal.total -= $scope.productsMap[p.id].price * p.quantity;
    $scope.modal.products.splice(idx, 1);
  };

  // Create invoice
  $scope.createInvoice = function() {
    InvoicesRes.post({
        customer_id: $scope.modal.customer,
        total: $scope.modal.total
    }).$promise.then(function(invoice) {
      var items = [];
      angular.forEach($scope.modal.products, function(p) {
        items.push(InvoiceItemsRes.post({
          invoice_id: invoice.id
        }, {
          invoice_id: invoice.id,
          product_id: p.id,
          quantity: p.quantity
        }));
      });

      return $q.all(items);
    }).then(function() {
      $scope.loading = true;
      $scope.closeModal();
      return InvoicesRes.get().$promise.then(function(result) {
        $scope.invoices = result;
        $scope.loading = false;
      });
    }).catch(function(err) {
      alert(err);
    });
  };

  // Get data from API
  $q.all([
    InvoicesRes.get().$promise.then(function(result) {
      $scope.invoices = result;
      console.dir(result);
    }),
    ProductsRes.get().$promise.then(function(result) {
      $scope.products = result;
      $scope.productsMap = {};
      angular.forEach(result, function(o) {
        $scope.productsMap[o.id] = o;
      });
    }),
    CustomersRes.get().$promise.then(function(result) {
      $scope.customers = result;
      $scope.customersMap = {};
      angular.forEach(result, function(o) {
        $scope.customersMap[o.id] = o;
      });
      console.dir($scope.customersMap);
    })
  ]).then(function() {
    $scope.loading = false;
  })

});
