angular.module('App').controller('InvoiceCtrl', function($rootScope, $routeParams, $location, $q, $scope,
  InvoicesRes, InvoiceItemsRes, ProductsRes, CustomersRes) {

  $scope.loading = true;
  $rootScope.page = 'invoices';

  // Get data from API
  $q.all([
    InvoicesRes.details({id: $routeParams.id}).$promise.then(function(result) {
      $scope.invoice = result;
      console.dir(result);
    }),
    InvoiceItemsRes.get({invoice_id: $routeParams.id}).$promise.then(function(result) {
      $scope.items = result;
      console.dir(result);
    }),
    ProductsRes.get().$promise.then(function(result) {
      $scope.products = result;
      $scope.productsMap = {};
      angular.forEach(result, function(o) { $scope.productsMap[o.id] = o; });
    }),
    CustomersRes.get().$promise.then(function(result) {
      $scope.customers = result;
      $scope.customersMap = {};
      angular.forEach(result, function(o) { $scope.customersMap[o.id] = o; });
    })
  ]).then(function() {
    $scope.loading = false;
  }).catch(function(err) {
    console.log(err);
  });

});
